
public class Aufgabe2 {

	public static void main(String[] args) {
	  double x = 10;
	  double y = 5;
	  System.out.println(mal(x,y));
	  double w = 3;
	  double k = 2;
      double z = 6;
      System.out.println(wuerfel(w));	  
      System.out.println(quader(w,k,z));
      System.out.println(pyramide(x,y));
      System.out.println(kugel(w));
      double aa = 2.36;
      double bb = 7.87;
      System.out.println("multi ergebniss = " + multi(aa,bb));
      double r1 = 3;
      double r2 = 4;
      System.out.println("Der Gesamtwiederstand in der Reihenschaltung betr�gt: " + reihenschaltung(r1,r2));
      System.out.println("Der Gesamtwiderstand in der Parallelschaltung betr�gt: " + parallelschaltung(r1,r2));
      System.out.println("x^2 =" + quadriert(x));
	

	}
	public static double mal(double a, double b) {
	  return a*b;
	}
	public static double wuerfel(double c) {
		return c*c*c;
	}
	public static double quader(double h, double n, double m) {
		return h*n*m;
	}
	public static double pyramide(double o, double u) {
		return o*o*u/3;
	}
	public static double kugel(double r) {
		return 4.0/3.0 * r*r*r * Math.PI;
	}
	public static double multi(double aa, double bb) {
		return aa * bb ;
	}
	public static double reihenschaltung(double r1, double r2) {
		return r1 + r2; 
	}
	public static double parallelschaltung(double r3,double r4) {
		return (r3 * r4)/(r3 + r4);
	}
	public static double quadriert(double x) {
		return x*x;
	}
	
}
