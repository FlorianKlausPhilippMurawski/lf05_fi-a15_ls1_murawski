import java.util.Scanner;
public class unterrischt_aufgaben1 {
	static Scanner tastatur = new Scanner(System.in);
	
	//Teil1
	public static void main(String[] args) {
	       
	       double zuZahlenderBetrag = fahrkartenbestellungErfassen(), r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	       
	       fahrkartenAusgeben();
	       rueckgeldAusgeben(r�ckgabebetrag);
	       
	       tastatur.close();

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
	                "vor Fahrtantritt entwerten zu lassen!\n" +
	                "Wir w�nschen Ihnen eine gute Fahrt.");
	    }

	
	//Teil2
	public static double fahrkartenbestellungErfassen() {
			double zuZahlenderBetrag;
	        int anzahlTickets;
	        
	        System.out.print("Zu zahlender Betrag (EURO): ");
		    zuZahlenderBetrag = tastatur.nextDouble();
		    if (zuZahlenderBetrag <= 0) {
		    System.out.print("kein G�ltiger Betrag, der Betrag wurde auf 1� gesetzt\"\n");
		    zuZahlenderBetrag = 1;
		    }
		    else {
	            System.out.println("G�ltiger Betrag, der Betrag wurde auf " + zuZahlenderBetrag + "� gesetzt" );
	           
		    	
		    } 
		    
		    
	        System.out.print("Ticket Anzahl: ");
	        
	        // Zuweisung des Werts
	        anzahlTickets = tastatur.nextInt();
	        
	        if (anzahlTickets <= 10 & anzahlTickets >= 1) {
	        System.out.print("G�ltige Menge an Tickets\n");
	        
	        }
	        else {
	            System.out.println("Keine G�ltige Anzahl an Tickets, die Menge der Tickets wurde auf 1 gesetzt.");
	            anzahlTickets = 1;
	        }
	       
	        
	        //Endbetrag
	        return zuZahlenderBetrag *anzahlTickets;
			}
	
			
	//Teil3
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
	        //Bezahlung
	 	    double eingezahlterGesamtbetrag = 0.0;
		    while(eingezahlterGesamtbetrag < zuZahlenderBetrag){
		    	
		    	   System.out.printf("%s%.2f%s" , "Noch zu zahlen: " , (zuZahlenderBetrag - eingezahlterGesamtbetrag) , (" Euro "));
		           System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
		           eingezahlterGesamtbetrag += tastatur.nextDouble();
		           
		       }
		       return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	        }

	
	//Teil4
	public static void fahrkartenAusgeben() {
	       // Fahrscheinausgabe
	       // -----------------
		   System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++){
	    	   
	          System.out.print("�����");
	          try {
				Thread.sleep(350);
				
			} catch (InterruptedException e) {
				
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }	       
		   System.out.println("\n\n");
		}

	
	//Teil5
	public static void rueckgeldAusgeben(double r�ckgabebetrag) {
	       // R�ckgeldberechnung und -Ausgabe
	       // -------------------------------
		
	       if(r�ckgabebetrag > 0.0){
	    	   System.out.printf("%s%.2f%s" , "Der R�ckgabebetrag in H�he von " , r�ckgabebetrag , " Euro ");
	    	   System.out.printf("wird in folgenden M�nzen ausgezahlt:\n");

	           while(r�ckgabebetrag >= 2.00) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
		          r�ckgabebetrag -= 2.00;
	           }
	           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
		          r�ckgabebetrag -= 1.00;
	           }
	           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          r�ckgabebetrag -= 0.5;
	           }
	           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          r�ckgabebetrag -= 0.2;
	           }
	           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          r�ckgabebetrag -= 0.1;
	           }
	           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          r�ckgabebetrag -= 0.05;
	           }
	       }

	       
	       tastatur.close();
	    
	

	}

}