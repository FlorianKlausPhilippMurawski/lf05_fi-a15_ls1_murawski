
public class temperaturtabelle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
    System.out.printf("%-12s| %10s\n", "Fahrenheit", "Celsius");
    System.out.printf("%-12d| %10.2f\n" , -20, -28.8889);
    System.out.printf("%-12d| %10.2f\n" , -10, -23.3333);
    System.out.printf("%-12d| %10.2f\n" , 0, -17.7778);
    System.out.printf("%-12d| %10.2f\n" , 20, -6.6667);
    System.out.printf("%-12d| %10.2f\n" , 30, -1.1111);
    
    System.out.printf("%-5s = %-18s = %4d\n", "0!", "", 1);
    System.out.printf("%-5s = %-18s = %4d\n", "1!", "1", 1);
    System.out.printf("%-5s = %-18s = %4d\n", "2!", "1 * 2", 2);
    System.out.printf("%-5s = %-18s = %4d\n", "3!", "1 * 2 * 3", 2 * 3);
    System.out.printf("%-5s = %-18s = %4d\n", "4!", "1 * 2 * 3 * 4", 2 * 3 * 4);
    System.out.printf("%-5s = %-18s = %4d\n", "5!", "1 * 2 * 3 * 4 * 5", 2 * 3 * 4 * 5);
    
    String m = "*";
	System.out.printf("%3s", m);
	System.out.printf("%3s\n", m);
	System.out.printf("%1s", m);
	System.out.printf("%7s\n", m);
	System.out.printf("%1s", m);
	System.out.printf("%7s\n", m);
	System.out.printf("%3s", m);
	System.out.printf("%3s\n", m);
	
	}

}
