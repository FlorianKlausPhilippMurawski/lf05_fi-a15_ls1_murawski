package datentypen;

/** Variablen.java
    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
    @author
    @version
*/
public class Variablen {
  public static void main(String [] args){
    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
          Vereinbaren Sie eine geeignete Variable */
	  int durchlaeufe ;
	  durchlaeufe = 25;
      System.out.println(durchlaeufe);
    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
          und geben Sie ihn auf dem Bildschirm aus.*/

    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
          eines Programms ausgewaehlt werden.
          Vereinbaren Sie eine geeignete Variable */
      char menupunkt;
      menupunkt = 'c';
    		  System.out.println(menupunkt);

    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/

    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
          notwendig.
          Vereinbaren Sie eine geeignete Variable */
    	long astronomisch ;
    	astronomisch = 299792458l;
    			System.out.println(astronomisch);
    		  

    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie sie auf dem Bildschirm aus.*/

    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/
    			byte mitarbeiter;
    			mitarbeiter = 7;
    			System.out.println(mitarbeiter);

    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/

    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
          dem Bildschirm aus.*/
    			double ladung ;
    			ladung = 1.602;
    			System.out.println("Die Elementar Ladung in 10^-19:" +ladung);
    			
       
    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */
    			double zahlung;
    			zahlung = 667.76;
    			System.out.println(zahlung);

    /*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variable den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/
    			
    System.out.printf("xx%-22sxx", "my name jeff");
  }//main
}// Variablen