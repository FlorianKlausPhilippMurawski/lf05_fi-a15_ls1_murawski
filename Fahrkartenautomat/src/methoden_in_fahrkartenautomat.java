import java.util.Scanner;

public class methoden_in_fahrkartenautomat {

	public static void main(String[] args) {
		double zuZahlen = fahrkartenbestellungErfassen();
		double uebrigerBetrag = fahrkartenBezahlen(zuZahlen);
        System.out.println("Der Gesamtbetrag Betr�gt: " + zuZahlen);
        System.out.println("Uebriger Betrag: " + uebrigerBetrag);
        fahrkartenAusgeben();
        rueckgeldAusgeben(uebrigerBetrag);
        

	}
	public static double fahrkartenbestellungErfassen() {
		 Scanner tastatur = new Scanner(System.in);
			System.out.println("Zu Zahlender Betrag: ");
			double zuZahlen = tastatur.nextDouble();
			System.out.println("Anzahl der Tickets: ");
			short anzahlTickets = tastatur.nextShort();
			return zuZahlen * anzahlTickets;
	}
	public static double fahrkartenBezahlen(double zuZahlen) {
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag;
		double eingeworfeneM�nze;
		 eingezahlterGesamtbetrag = 0.0;
	        while(eingezahlterGesamtbetrag < zuZahlen)
	        {
	            System.out.println("Noch zu zahlen: " + String.format("%.2f EURO", zuZahlen - eingezahlterGesamtbetrag));
	            System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	            eingeworfeneM�nze = tastatur.nextDouble();
	            eingezahlterGesamtbetrag += eingeworfeneM�nze;
	        }
		return eingezahlterGesamtbetrag - zuZahlen ;
	}
	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            warte(250);
            
        }
	}
        public static void warte(int milli) {
        	try {
                Thread.sleep(milli);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        
	
	public static void rueckgeldAusgeben(double r�ckgabebetrag ) {
        if(r�ckgabebetrag > 0.0)
        {
            System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
            System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
                System.out.println("2 EURO");
                r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
                System.out.println("1 EURO");
                r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
                System.out.println("50 CENT");
                r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
                System.out.println("20 CENT");
                r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
                System.out.println("10 CENT");
                r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
                System.out.println("5 CENT");
                r�ckgabebetrag -= 0.05;
            }

		
        }
	}
}	
